package Discord;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

public class Command {
    public static void onCommand(IMessage message, String prefix){
        IUser sender = message.getAuthor();
        IChannel channel = message.getChannel();
        IGuild guild = message.getGuild();
        String[] command = message.getContent().replaceFirst(prefix, "").split(" ");
        if (command[0].equals("test")){
            sender.getOrCreatePMChannel().sendMessage("テスト。");
        }
    }
}
