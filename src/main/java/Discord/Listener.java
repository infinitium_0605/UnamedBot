package Discord;

import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class Listener {
    public static String prefix = new String("=");

    @EventSubscriber
    public void onMessageEvent(MessageReceivedEvent event) {
        if (event.getMessage().getContent().startsWith(prefix)) {
            if (!(event.getAuthor().isBot())) {
                Command.onCommand(event.getMessage(), prefix);
            }
        }
    }
}
